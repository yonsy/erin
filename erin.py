#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, render_template, request
from jinja2 import Environment, FileSystemLoader

import sys

app = Flask(__name__)


@app.route('/')
def hello():
    python_version = sys.version
    return render_template('index.html',
                           **locals(),)


@app.route('/home')
def home():
    from_home = True
    python_version = sys.version
    return render_template(
        'index.html',
        **locals(),)


if __name__ == '__main__':
    app.run()
